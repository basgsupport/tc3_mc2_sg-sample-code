﻿---

## About

This is a sample code on Tc3_MC2_SG library.

---

## How to use

Install Tc3_MC2_SG and Tc3_System_SG libraries in the folder.

How to install TwinCAT libraries: https://docs.google.com/document/d/1rGQ72qj_dWIp9lQ_pejA64TPPL7urPDjjBiHyGjCtus/edit?usp=sharing


Follow the instructions in the sample code.

---

## Help

Contact support@beckhoff.com.sg